#include "PerceptionManager.h"

#define DEBUG_MODE 1

PerceptionManager::PerceptionManager(void)
{
    submission_type = 0;
    submission_state = 4;
    camera_pitch_angle = 0.0;
}


PerceptionManager::~PerceptionManager(void)
{
}


void PerceptionManager::init(const std::string configFile)
{
    readConfigs(configFile);
    listOfCylindricalObjects.init(configFile);
    current_objects_state = listOfCylindricalObjects.getObjectsState();
    printCurrentObjectsState();
}

bool PerceptionManager::readConfigs(const std::string &configFile)
{
    pugi::xml_document doc;

    std::ifstream nameFile(configFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if(!result)
    {
        cout << "ERROR: Could not load the file: " << result.description() << endl;
        return 0;
    }


    pugi::xml_node Configuration = doc.child("Perception_Manager_config");
    std::string readingValue;

    readingValue = Configuration.child("front_camera_angle").child_value("pitch");
    camera_pitch_angle = atof(readingValue.c_str());
    cout<<"camera_pitch_angle: "<<camera_pitch_angle<<endl;
    camera_pitch_angle = (double)(camera_pitch_angle * M_PI)/180;

    readingValue = Configuration.child("front_camera_offset").child_value("x");
    camera_offset(0) = atof(readingValue.c_str());
    readingValue = Configuration.child("front_camera_offset").child_value("y");
    camera_offset(1) = atof(readingValue.c_str());
    readingValue = Configuration.child("front_camera_offset").child_value("z");
    camera_offset(2) = atof(readingValue.c_str());
    cout<<"Camera Offset"<<endl<<"x: "<<camera_offset(0)<<" ; y: "<<camera_offset(1)<<" ; z: "<<camera_offset(2)<<endl;

    readingValue = Configuration.child_value("max_detections_along_time");
    MAX_OBJECT_DETECTIONS_ALONG_TIME = atoi(readingValue.c_str());

}

void PerceptionManager::printCurrentObjectsState()
{
    cout<<"*** Current Objects State *** "<<endl;
    for(int i=0;i<current_objects_state.size();i++)
    {
        cout<<"<";
        for(int j=0;j<current_objects_state[i].size();j++)
        {
            if(j<current_objects_state[i].size()-1)
                cout<<current_objects_state[i][j]<< " , ";
            else
                cout<<current_objects_state[i][j]<< " >"<<endl;
        }
    }
}

Eigen::Matrix3d PerceptionManager::convertFromRPYtoRotationMatrix(const float alpha, const float beta, const float gamma)
{
    Eigen::Matrix3d R;
    R(0,0) = cos(alpha)*cos(beta);
    R(0,1) = cos(alpha)*sin(beta)*sin(gamma) - sin(alpha)*cos(gamma);
    R(0,2) = cos(alpha)*sin(beta)*cos(gamma) + sin(alpha)*sin(gamma);

    R(1,0) = sin(alpha)*cos(beta);
    R(1,1) = sin(alpha)*sin(beta)*sin(gamma) + cos(alpha)*cos(gamma);
    R(1,2) = sin(alpha)*sin(beta)*cos(gamma) - cos(alpha)*sin(gamma);

    R(2,0) = -sin(beta);
    R(2,1) = cos(beta)*sin(gamma);
    R(2,2) = cos(beta)*cos(gamma);

    return R;

}

Eigen::Matrix4d PerceptionManager::computeHomogeneousMatrixTransform(const float px, const float py, const float pz, const float alpha, const float beta, const float gamma)
{
    //Eigen::Matrix4d T_uav_world;
    Eigen::Matrix3d R = convertFromRPYtoRotationMatrix(alpha, beta, gamma);

    T_uav_world(0,0) = R(0,0); T_uav_world(0,1) = R(0,1); T_uav_world(0,2) = R(0,2); T_uav_world(0,3) = px;
    T_uav_world(1,0) = R(1,0); T_uav_world(1,1) = R(1,1); T_uav_world(1,2) = R(1,2); T_uav_world(1,3) = py;
    T_uav_world(2,0) = R(2,0); T_uav_world(2,1) = R(2,1); T_uav_world(2,2) = R(2,2); T_uav_world(2,3) = pz;
    T_uav_world(3,0) = 0.0; T_uav_world(3,1) = 0.0; T_uav_world(3,2) = 0.0; T_uav_world(3,3) = 1.0;

    //std::cout<<"homogeneous Transform (T_uav_world): "<<std::endl<<T_uav_world<<std::endl<<std::endl;
    return T_uav_world;


}

Eigen::Matrix4d PerceptionManager::computeHomogeneousMatrixTransform(const float px, const float py, float pz, Eigen::Quaterniond q_E)
{
    Eigen::Matrix3d Rmat_E;
    Rmat_E = q_E.toRotationMatrix();

    T_obj_cam(0,0) = Rmat_E(0,0); T_obj_cam(0,1) = Rmat_E(0,1); T_obj_cam(0,2) = Rmat_E(0,2); T_obj_cam(0,3) = px;
    T_obj_cam(1,0) = Rmat_E(1,0); T_obj_cam(1,1) = Rmat_E(1,1); T_obj_cam(1,2) = Rmat_E(1,2); T_obj_cam(1,3) = py;
    T_obj_cam(2,0) = Rmat_E(2,0); T_obj_cam(2,1) = Rmat_E(2,1); T_obj_cam(2,2) = Rmat_E(2,2); T_obj_cam(2,3) = pz;
    T_obj_cam(3,0) = 0.0; T_obj_cam(3,1) = 0.0; T_obj_cam(3,2) = 0.0; T_obj_cam(3,3) = 1.0;

    //    cout<<"Quaternion: "<<"["<<q_E.x()<<", "<<q_E.y()<<", "<<q_E.z()<<", "<<q_E.w()<<"]"<<endl;
    //    cout<<"Rot Mat from Quaternion: "<<endl<<Rmat_E<<endl;

    return T_obj_cam;

}

Eigen::Matrix4d PerceptionManager::computeObjectToWorldTransform()
{
    if(submission_state == SubmissionState::GoForReleasingBlueObject || submission_state == SubmissionState::GoForReleasingRedObject)
    {
        std::cout<<"inside pitch angle"<<std::endl;
        camera_pitch_angle = -90;
    }
    Eigen::Matrix4d T_rot_90_Zw;
    T_rot_90_Zw(0,0) = 0.0; T_rot_90_Zw(0,1) = 1.0; T_rot_90_Zw(0,2) = 0.0; T_rot_90_Zw(0,3) = 0.0;
    T_rot_90_Zw(1,0) = -1.0; T_rot_90_Zw(1,1) = 0.0; T_rot_90_Zw(1,2) = 0.0; T_rot_90_Zw(1,3) = 0.0;
    T_rot_90_Zw(2,0) = 0.0; T_rot_90_Zw(2,1) = 0.0; T_rot_90_Zw(2,2) = 1.0; T_rot_90_Zw(2,3) = 0.0;
    T_rot_90_Zw(3,0) = 0.0; T_rot_90_Zw(3,1) = 0.0; T_rot_90_Zw(3,2) = 0.0; T_rot_90_Zw(3,3) = 1.0;


    Eigen::Matrix4d T_rot_90_Xw;
    T_rot_90_Xw(0,0) = 1.0; T_rot_90_Xw(0,1) = 0.0; T_rot_90_Xw(0,2) = 0.0; T_rot_90_Xw(0,3) = 0.0;
    T_rot_90_Xw(1,0) = 0.0; T_rot_90_Xw(1,1) = 0.0; T_rot_90_Xw(1,2) = 1.0; T_rot_90_Xw(1,3) = 0.0;
    T_rot_90_Xw(2,0) = 0.0; T_rot_90_Xw(2,1) = -1.0; T_rot_90_Xw(2,2) = 0.0; T_rot_90_Xw(2,3) = 0.0;
    T_rot_90_Xw(3,0) = 0.0; T_rot_90_Xw(3,1) = 0.0; T_rot_90_Xw(3,2) = 0.0; T_rot_90_Xw(3,3) = 1.0;


    Eigen::Matrix4d T_offset;
    T_offset(0,0) = 1.0; T_offset(0,1) = 0.0; T_offset(0,2) = 0.0; T_offset(0,3) = camera_offset(0);
    T_offset(1,0) = 0.0; T_offset(1,1) = 1.0; T_offset(1,2) = 0.0; T_offset(1,3) = camera_offset(1);
    T_offset(2,0) = 0.0; T_offset(2,1) = 0.0; T_offset(2,2) = 1.0; T_offset(2,3) = camera_offset(2);
    T_offset(3,0) = 0.0; T_offset(3,1) = 0.0; T_offset(3,2) = 0.0; T_offset(3,3) = 1.0;


    Eigen::Matrix4d T_rot_angle_Zw;
    T_rot_angle_Zw(0,0) = 1.0; T_rot_angle_Zw(0,1) = 0.0; T_rot_angle_Zw(0,2) = 0.0; T_rot_angle_Zw(0,3) = 0.0;
    T_rot_angle_Zw(1,0) = 0.0; T_rot_angle_Zw(1,1) = cos(camera_pitch_angle); T_rot_angle_Zw(1,2) = -sin(camera_pitch_angle); T_rot_angle_Zw(1,3) = 0.0;
    T_rot_angle_Zw(2,0) = 0.0; T_rot_angle_Zw(2,1) = sin(camera_pitch_angle); T_rot_angle_Zw(2,2) = cos(camera_pitch_angle); T_rot_angle_Zw(2,3) = 0.0;
    T_rot_angle_Zw(3,0) = 0.0; T_rot_angle_Zw(3,1) = 0.0; T_rot_angle_Zw(3,2) = 0.0; T_rot_angle_Zw(3,3) = 1.0;


    Eigen::Matrix4d T_cam_world = T_uav_world * T_offset *  T_rot_90_Zw * T_rot_90_Xw * T_rot_angle_Zw;
    T_obj_world = T_cam_world * T_obj_cam;


    if(DEBUG_MODE)
    {
        std::cout<<"homogeneous Transform (T_obj_cam): "<<std::endl<<T_obj_cam<<std::endl<<std::endl;
        std::cout<<"homogeneous Transform (T_rot_90_Zw): "<<std::endl<<T_rot_90_Zw<<std::endl<<std::endl;
        std::cout<<"homogeneous Transform (T_rot_90_Xw): "<<std::endl<<T_rot_90_Xw<<std::endl<<std::endl;
        std::cout<<"homogeneous Transform (T_rot_angle_Zw): "<<std::endl<<T_rot_angle_Zw<<std::endl<<std::endl;
        std::cout<<"homogeneous Transform (T_cam_world): "<<std::endl<<T_cam_world<<std::endl<<std::endl;
        std::cout<<"************* homogeneous Transform (T_obj_world) ************* "<<std::endl<<T_obj_world<<std::endl<<std::endl;
    }


    return T_obj_world;

}


void PerceptionManager::updateObjectState(const unsigned int object_id, const std::vector<int> &new_state)
{
    listOfCylindricalObjects.updateObjectState(object_id, new_state);
    current_objects_state = listOfCylindricalObjects.getObjectsState();
    printCurrentObjectsState();
    generateSubmission(object_id, new_state);
}

void PerceptionManager::updateObjectPose(const unsigned int object_id)
{
    Eigen::Vector3d Tmat;
    Eigen::Matrix3d Rmat;
    Rmat = T_obj_world.block<3,3>(0,0);
    Tmat = T_obj_world.block<3,1>(0,3);

    Eigen::Quaterniond q_E(Rmat);
    listOfCylindricalObjects.updateObjectPose(object_id, Tmat, q_E);
}

void PerceptionManager::printNumObjectsDetectionsAlongTime()
{
    num_objects_detections_along_time = listOfCylindricalObjects.getNumObjectsDetectionsAlongTime();

    std::cout<<"----- Num Objects Detections Along Time -----"<<std::endl;
    for(unsigned int i=0;i<num_objects_detections_along_time.size();i++)
    {
        cout<<"<";
        if(i<num_objects_detections_along_time.size()-1)
            std::cout<<num_objects_detections_along_time[i]<< " , ";
        else
            std::cout<<num_objects_detections_along_time[i]<< " >"<<endl;
    }
}

bool PerceptionManager::validateObjectPose(const int object_id, const Eigen::Matrix4d &Tmat)
{

    bool correct_pose_based_on_height = true;

    double object_height = Tmat(2,3);
    cout<<"Object Height: "<<object_height<<endl;


    //Height of Item must be between (55 - 85 cm)
    if((object_id == 0) || (object_id == 2)) //Items
    {
        if((object_height < (MAX_SURFACE_HEIGHT - TOLERANCE_HEIGHT)) || ((object_height > (MAX_SURFACE_HEIGHT + TOLERANCE_HEIGHT))))
            correct_pose_based_on_height = false;
    }
    //Height of Bucket must be between (0 - 30 cm)
    else if((object_id == 1) || (object_id == 3)) //Buckets
    {
        if((object_height < -0.35) || ((object_height > 1.5)))
            correct_pose_based_on_height = false;

    }

    return correct_pose_based_on_height;
}

void PerceptionManager::generateSubmission(const unsigned int object_id, const std::vector<int> &new_state)
{
    //All the Logic of the Percption Manager goes here


    std::vector<bool> recognized_state;
    std::vector<bool> picked_state;
    std::vector<bool> released_state;
    for(int i=0;i<current_objects_state.size();i++)
    {
        recognized_state.push_back(current_objects_state[i][0]);
        picked_state.push_back(current_objects_state[i][1]);
        released_state.push_back(current_objects_state[i][2]);
    }

    //ItemA -> object_id == 0
    //BucketA -> object_id == 1
    //ItemB -> object_id == 2
    //BucketB -> object_id == 3

    if((object_id == 0) || (object_id == 2)) //Logic for ItemA and ItemB
    {
        float sum_picked_state = std::accumulate(picked_state.begin(), picked_state.end(), 0);
        if(new_state[0] == 1) //Object (Item) RECOGNIZED
        {
            if(sum_picked_state == 0) //If Item Detected && NO object is Picked
            {
                submission_type = SubmissionType::GoForGrasping;
                if(object_id == 0)
                    submission_state = SubmissionState::GoForGraspingRedObject;
                else if(object_id == 2)
                    submission_state = SubmissionState::GoForGraspingBlueObject;

                //Create the MoveToPoint w.r.t the position of the RECOGNIZED OBJECT for GOING to GRASP
                Eigen::Vector3d object_position = listOfCylindricalObjects.getObjectPosition(object_id);
                cout<<"Next Mission: GO FOR GRASPING"<<endl;
            }
            else
            {
                submission_state = SubmissionState::Exploring;

                //Create the MoveToPoint w.r.t the position of the RECOGNIZED OBJECT for SAVING the POSITION
                Eigen::Vector3d object_position = listOfCylindricalObjects.getObjectPosition(object_id);
                cout<<"Next Mission: NOT GOING FOR GRASPING -- Object NOT Picked"<<endl;
            }
        }

        if(new_state[1] == 1) //Object PICKED
        {
            if(recognized_state[object_id + 1]) //Object (Bucket) RECOGNIZED
            {
                submission_type = SubmissionType::GoForReleasing;

                if(object_id == 1)
                    submission_state = SubmissionState::GoForReleasingRedObject;
                else if(object_id == 3)
                    submission_state = SubmissionState::GoForReleasingBlueObject;
                //Eigen::Vector3d object_position = listOfCylindricalObjects.getObjectPosition(object_id + 1);
                listOfCylindricalObjects.computeObjectDefinitivePosition(object_id + 1);
                cout<<"Next Mission: GO FOR RELEASING Picked Object"<<endl;
            }
            else
            {
                submission_state = SubmissionState::Exploring;
            }

        }

        if(new_state[2] == 1) //Object RELEASED
        {
            if(object_id == 0) //Released Red Item
            {
                if(recognized_state[2]) //Recognized Blue Item
                {
                    submission_type = SubmissionType::GoForGrasping;
                    submission_state = SubmissionState::GoForGraspingBlueObject;
                    Eigen::Vector3d object_pos = listOfCylindricalObjects.getObjectPosition(2);
                }
                else
                {
                    submission_state = SubmissionState::Exploring;
                }
                cout<<"Item RED RELEASED"<<endl;
            }
            else if(object_id == 2) //Released Blue Item
            {
                if(recognized_state[0]) //Recognized Red Item
                {
                    submission_type = SubmissionType::GoForGrasping;
                    submission_state = SubmissionState::GoForGraspingRedObject;
                    Eigen::Vector3d object_pos = listOfCylindricalObjects.getObjectPosition(0);
                }
                else
                {
                    submission_state = SubmissionState::Exploring;
                }
                cout<<"Item BLUE RELEASED"<<endl;
            }
            else
            {
                submission_state = SubmissionState::Exploring;
            }

        }

    }

    else if((object_id == 1) || (object_id == 3)) //Logic for BucketA and BucketB
    {
        if(new_state[0] == 1) //Object (Bucket) RECOGNIZED
        {
            if(picked_state[object_id - 1] == 1) //An Item of the same color has been Picked
            {
                submission_type = SubmissionType::GoForReleasing;

                if(object_id == 1)
                    submission_state = SubmissionState::GoForReleasingRedObject;
                else if(object_id == 3)
                    submission_state = SubmissionState::GoForReleasingBlueObject;

                //Create the MoveToPoint w.r.t the position of the RECOGNIZED OBJECT for GOING to GRASP
                //Eigen::Vector3d object_position = listOfCylindricalObjects.getObjectPosition(object_id);
                listOfCylindricalObjects.computeObjectDefinitivePosition(object_id);
                cout<<"Next Mission: GO FOR RELEASING"<<endl;
            }
            else
            {
                submission_state = SubmissionState::Exploring;
                //Create the MoveToPoint w.r.t the position of the RECOGNIZED OBJECT for SAVING the POSITION
                Eigen::Vector3d object_position = listOfCylindricalObjects.getObjectPosition(object_id);
            }
        }
    }
}



