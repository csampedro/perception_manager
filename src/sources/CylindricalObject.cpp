#include "CylindricalObject.h"

#define DEBUG_MODE 0

inline bool operator<(const Eigen::Vector3d &lhs, const Eigen::Vector3d &rhs)
{
    return lhs.x() < rhs.x() && lhs.y() < rhs.y() && lhs.z() < rhs.z();
}

inline bool cmpVecs(const Eigen::Vector3d &lhs, const Eigen::Vector3d &rhs)
{
    return lhs.x() < rhs.x() && lhs.y() < rhs.y() && lhs.z() < rhs.z();
}

inline double dist(const Eigen::Vector3d &pos, const Eigen::Vector3d mean_pos)
{
    Eigen::Vector3d diff = pos - mean_pos;
    double dist = diff.squaredNorm();
    return dist;
}

struct DistanceFunc
{
    DistanceFunc(const Eigen::Vector3d& _p) : p(_p) {}

    bool operator()(const Eigen::Vector3d& lhs, const Eigen::Vector3d& rhs) const
    {
        return dist(lhs, p) < dist(rhs, p);
    }

private:
    Eigen::Vector3d p;
};



CylindricalObject::CylindricalObject(void)
{
    num_object_states = 3; //{Recognized, Picked, Released}
    object_state.clear();
    for(unsigned int i=0;i<num_object_states;i++)
        object_state.push_back(0);
}

CylindricalObject::CylindricalObject(float r, float h, unsigned int id, string color)
{
    num_object_states = 3; //{Recognized, Picked, Released}
    radius = r;
    height = h;
    object_id = id;
    object_color = color;

    object_state.clear();
    for(unsigned int i=0;i<num_object_states;i++)
        object_state.push_back(0);
}

CylindricalObject::CylindricalObject(float r, float h, unsigned int id, string color, std::vector<bool> default_states)
{
    num_object_states = default_states.size(); //{Recognized, Picked, Released}
    radius = r;
    height = h;
    object_id = id;
    object_color = color;

    object_state.clear();
    for(unsigned int i=0;i<num_object_states;i++)
        object_state.push_back(default_states[i]);
}

CylindricalObject::~CylindricalObject(void)
{
}

void CylindricalObject::printObjectState()
{
    cout<<"<";
    for(unsigned int i=0;i<object_state.size();i++)
    {
        if(i<object_state.size()-1)
            cout<<object_state[i]<<", ";
        else
            cout<<object_state[i]<<">"<<endl;

    }
}

void CylindricalObject::setPosition(double x, double y, double z)
{
    position(0) = x;
    position(1) = y;
    position(2) = z;
}

void CylindricalObject::computeDefinitivePosition()
{
    Eigen::Vector3d median_position;
    Eigen::Vector3d mean_position;
    Eigen::Vector3d best_measurement;
    if(positions_along_time.size()>2)
    {
        //median_position = computeMedian();
        best_measurement = computeBestMeasurement(positions_along_time);
        position = best_measurement;
    }

    mean_position = computeMean(positions_along_time);
    cout<<endl<<"---- Mean Position: ----"<<endl;
    cout<<"<"<<mean_position(0)<<", "<<mean_position(1)<<", "<<mean_position(2)<<">"<<endl<<endl;

    best_measurement = computeBestMeasurement(positions_along_time);
    cout<<endl<<"---- Best Measurement ----"<<endl;
    cout<<"<"<<best_measurement(0)<<", "<<best_measurement(1)<<", "<<best_measurement(2)<<">"<<endl<<endl;
}

Eigen::Vector3d CylindricalObject::getPosition()
{
//    Eigen::Vector3d median_position;
//    if(positions_along_time.size()>2)
//    {
//        median_position = computeMedian();
//        position = median_position;
//    }
//    else
//        median_position = position;

////    for(unsigned int i=0;i<positions_along_time.size();i++)
////        cout<<"<"<<positions_along_time[i](0)<<", "<<positions_along_time[i](1)<<", "<<positions_along_time[i](2)<<">"<<endl;
//    cout<<"---- Median Position: ----"<<endl;
//    cout<<"<"<<median_position(0)<<", "<<median_position(1)<<", "<<median_position(2)<<">"<<endl;

//    return median_position;

    return position;
}

void CylindricalObject::updateObjectState(const std::vector<int> &state)
{
    //The vector state is of the form <-1, -1, ..., 1, -1> or <-1, -1, ..., 0, -1>
    //Where the position of the vector !=-1 is the flag of the Object that
    //has to be UPDATED.
    for(int i=0;i<state.size();i++)
    {
        if(state[i] != -1)
            object_state[i] = state[i];
    }
}

void CylindricalObject::updateObjectPose(const Eigen::Vector3d p_E, const Eigen::Quaterniond q_E)
{
    positions_along_time.push_back(p_E);
    if(DEBUG_MODE)
    {
        std::cout<<"--- Object: "<<object_id<<"---"<<std::endl;
        cout<<"<"<<position(0)<<", "<<position(1)<<", "<<position(2)<<">"<<endl;
    }

    position = p_E;
    orientation = q_E;

    if(positions_along_time.size() > MAX_OBJECT_POSES_ALONG_TIME)
        positions_along_time.resize(MAX_OBJECT_POSES_ALONG_TIME/2);
}


void CylindricalObject::resetObjectPositionsAlongTime()
{
    positions_along_time.clear();
}

Eigen::Vector3d CylindricalObject::computeMedian()
{
    std::vector<Eigen::Vector3d> positions_along_time_med;

    //Point for Debbuging purposes
//    Eigen::Vector3d p1(4.33, -0.99, 0.25);
//    Eigen::Vector3d p2(4.24, -0.92, 0.156);
//    Eigen::Vector3d p3(4.78, 1.7, 0.44);
//    Eigen::Vector3d p4(3.78, -1.9, 0.014);
//    Eigen::Vector3d p5(4.14, -1.8, 0.105);
//    Eigen::Vector3d p6(4.06, -1.109, 0.114);
//    positions_along_time_med.push_back(p1);
//    positions_along_time_med.push_back(p2);
//    positions_along_time_med.push_back(p3);
//    positions_along_time_med.push_back(p4);
//    positions_along_time_med.push_back(p5);
//    positions_along_time_med.push_back(p6);
//    std::sort(positions_along_time_med.data(), positions_along_time_med.data() + positions_along_time_med.size(), cmpVecs);


    Eigen::Vector3d median;
    size_t size = positions_along_time.size();

    std::sort(positions_along_time.data(), positions_along_time.data() + positions_along_time.size(), cmpVecs);

    if (size  % 2 == 0)
    {
        median = (positions_along_time[size / 2 - 1] + positions_along_time[size / 2]) / 2;
    }
    else
    {
        median = positions_along_time[size / 2];
    }

    return median;
}

Eigen::Vector3d CylindricalObject::computeMean(const std::vector<Eigen::Vector3d> &vec_pos)
{
    Eigen::Vector3d accumulated_pos = std::accumulate(vec_pos.begin(), vec_pos.end(), Eigen::Vector3d::Zero().eval());
    Eigen::Vector3d mean_pos = accumulated_pos/vec_pos.size();

    return mean_pos;
}

Eigen::Vector3d CylindricalObject::computeBestMeasurement(const std::vector<Eigen::Vector3d> &vec_pos)
{
    int num_update_steps = 10;

    std::vector<Eigen::Vector3d> copyVec_pos(vec_pos);
    Eigen::Vector3d mean_pos;
    for(unsigned int i=0; i< num_update_steps;i++)
    {
        mean_pos = computeMean(copyVec_pos);
        std::sort(copyVec_pos.begin(), copyVec_pos.end(), DistanceFunc(mean_pos));

        int resize_factor = (int)(copyVec_pos.size()*0.8);
        if(resize_factor > 0)
            copyVec_pos.resize(resize_factor);
    }

    //return copyVec_pos[0];
    return mean_pos;


//    std::vector<Eigen::Vector3d> vector_prueba;
//    vector_prueba.push_back(Eigen::Vector3d(2.99, -0.09, 0.25));
//    vector_prueba.push_back(Eigen::Vector3d(2.99, -0.09, 0.25));
//    vector_prueba.push_back(Eigen::Vector3d(2.95, -0.03, 0.156));
//    vector_prueba.push_back(Eigen::Vector3d(2.89, 0.035, 0.24));
//    vector_prueba.push_back(Eigen::Vector3d(3.08, -0.09, 0.14));
//    vector_prueba.push_back(Eigen::Vector3d(4.14, -0.1, 0.105));
//    vector_prueba.push_back(Eigen::Vector3d(4.06, -1.109, 0.114));
//    vector_prueba.push_back(Eigen::Vector3d(2.06, 0.37, 0.114));
//    vector_prueba.push_back(Eigen::Vector3d(2.845, 0.07, 0.114));
//    vector_prueba.push_back(Eigen::Vector3d(2.898, 0.12, 0.114));


//    for(unsigned int i=0;i<num_update_steps;i++)
//    {
//        Eigen::Vector3d mean_pos_prueba = computeMean(vector_prueba);
//        std::sort(vector_prueba.begin(), vector_prueba.end(), DistanceFunc(mean_pos_prueba));

//        cout<<endl<<"---- Mean Position Prueba ----"<<endl;
//        cout<<"<"<<mean_pos_prueba(0)<<", "<<mean_pos_prueba(1)<<", "<<mean_pos_prueba(2)<<">"<<endl<<endl;

//        cout<<endl<<"^^^^^^^^ Point Sorted ^^^^^^^^^^"<<endl;
//        for(unsigned int j=0;j<vector_prueba.size();j++)
//            cout<<vector_prueba[j](0)<<" , "<<vector_prueba[j](1)<<" , "<<vector_prueba[j](2)<<endl;
//        cout<<endl;

//        vector_prueba.resize(vector_prueba.size()/2);

//    }

}
