#include "ListOfCylindricalObjects.h"

ListOfCylindricalObjects::ListOfCylindricalObjects()
{
    for(int i=0; i<MAX_OBJECTS; i++)
        objects_list[i]=0;

    number_of_objects=0;
}

ListOfCylindricalObjects::~ListOfCylindricalObjects()
{
    DestroyContent();
}

void ListOfCylindricalObjects::init(string configFile)
{
    readConfigs(configFile);
}

bool ListOfCylindricalObjects::readConfigs(std::string &configFile)
{
    pugi::xml_document doc;

    std::ifstream nameFile(configFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if(!result)
    {
        cout << "ERROR: Could not load the file: " << result.description() << endl;
        return 0;
    }


    pugi::xml_node Configuration = doc.child("Perception_Manager_config");
    std::string readingValue;

    int numOfObjects = 0;
    pugi::xml_node nodeObjects = Configuration.child("ListOfCylindricalObjects");
    cout<<"***** OBJECT PARAMETERS *****"<<endl;
    for(pugi::xml_node nodeObject = nodeObjects.first_child(); nodeObject; nodeObject = nodeObject.next_sibling())
    {

        readingValue = nodeObject.child_value("radius");
        double radius = atof(readingValue.c_str());
        readingValue = nodeObject.child_value("height");
        double height = atof(readingValue.c_str());
        std::string color = nodeObject.child_value("color");
        readingValue = nodeObject.child_value("id");
        unsigned int object_id = (unsigned int)atoi(readingValue.c_str());
        numOfObjects += 1;

        std::vector<bool> obj_states;
        pugi::xml_node nodeObjStates = nodeObject.child("defaultState");
        for(pugi::xml_node ch_node = nodeObjStates.first_child(); ch_node; ch_node = ch_node.next_sibling())
        {
            readingValue = ch_node.child_value();
            cout<<"readingValue: "<<readingValue<<endl;
            bool object_state = (bool)atoi(readingValue.c_str());
            obj_states.push_back(object_state);
        }

        cout<<"Object"<<numOfObjects<<" Params: "<<endl;
        cout<<"Radius: "<<radius<<endl;
        cout<<"Height: "<<height<<endl;
        cout<<"Color: "<<color<<endl;
        cout<<"Object Id: "<<object_id<<endl;
        cout<<"Object State: "<<endl;
        for(unsigned int i=0;i<obj_states.size();i++)
            cout<<obj_states[i]<<" , ";
        cout<<endl<<endl;


        //CylindricalObject *object = new CylindricalObject(radius, height, object_id, color);
        CylindricalObject *object = new CylindricalObject(radius, height, object_id, color, obj_states);
        Add(object);
    }


    return true;
}

bool ListOfCylindricalObjects::Add(CylindricalObject *d)
{
    cout<<"Adding object..."<<endl;
    if(number_of_objects < MAX_OBJECTS)
    {
        objects_list[number_of_objects++] = d;
        return true;
    }
    else
        return false;

}

void ListOfCylindricalObjects::Delete(CylindricalObject *d)
{
    for(unsigned int i=0; i<number_of_objects; i++)
    {
        if(objects_list[i] == d)
        {
            Delete(i);
            return;
        }
    }
}

void ListOfCylindricalObjects::Delete(int index)
{
    if(index<0 || index>=number_of_objects)
        return;
    else
    {
        delete objects_list[index];
        number_of_objects--;
        for(unsigned int i=index; i<number_of_objects; i++)
            objects_list[i] = objects_list[i+1];
    }
}


void ListOfCylindricalObjects::updateObjectState(const unsigned int object_id, const std::vector<int> &state)
{
    for(unsigned int i=0;i<number_of_objects;i++)
    {
        if(objects_list[i]->getObjectId() == object_id)
            objects_list[i]->updateObjectState(state);
    }
}

void ListOfCylindricalObjects::resetObjectDetectionsAlongTime(const unsigned int object_id)
{
    for(unsigned int i=0;i<number_of_objects;i++)
    {
        if(objects_list[i]->getObjectId() == object_id)
            objects_list[i]->resetObjectPositionsAlongTime();
    }
}

void ListOfCylindricalObjects::updateObjectPose(const unsigned int object_id, const Eigen::Vector3d p_E, const Eigen::Quaterniond q_E)
{
    for(unsigned int i=0;i<number_of_objects;i++)
    {
        if(objects_list[i]->getObjectId() == object_id)
            objects_list[i]->updateObjectPose(p_E, q_E);
    }
}

void ListOfCylindricalObjects::computeObjectDefinitivePosition(const unsigned int object_id)
{
    for(unsigned int i=0;i<number_of_objects;i++)
    {
        if(objects_list[i]->getObjectId() == object_id)
            objects_list[i]->computeDefinitivePosition();
    }
}

std::vector<bool> ListOfCylindricalObjects::getObjectState(const unsigned int object_id)
{
    return objects_list[object_id]->getObjectState();
}


std::vector<unsigned int> ListOfCylindricalObjects::getNumObjectsDetectionsAlongTime()
{
    std::vector<unsigned int> num_detections_along_time;
    for(unsigned int i=0;i<number_of_objects;i++)
        num_detections_along_time.push_back(objects_list[i]->getNumDetectionsAlongTime());

    return num_detections_along_time;
}


std::vector<std::vector<bool> > ListOfCylindricalObjects::getObjectsState()
{
    std::vector<std::vector<bool> > objects_state;
    for(unsigned int i=0;i<number_of_objects;i++)
    {
        std::vector<bool> obj_state;
        obj_state = objects_list[i]->getObjectState();
        objects_state.push_back(obj_state);
    }
    return objects_state;
}

void ListOfCylindricalObjects::printObjectsState()
{
    cout<<"Printing Objects States..."<<endl;
    cout<<"number_of_objects: "<<number_of_objects<<endl;
    for(unsigned int i=0;i<number_of_objects;i++)
        objects_list[i]->printObjectState();
}

void ListOfCylindricalObjects::DestroyContent()
{
    for(unsigned int i=0; i<number_of_objects; i++)
        delete objects_list[i];
    number_of_objects = 0;
}
