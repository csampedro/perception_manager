#include "PerceptionManagerROSModule.h"
using namespace std;

PerceptionManagerROSModule::PerceptionManagerROSModule()
{
    droneId = DroneModule::idDrone;
}


PerceptionManagerROSModule::~PerceptionManagerROSModule()
{

}

void PerceptionManagerROSModule::init()
{
    cout<<"Initializing PerceptionManagerROSModule..."<<endl;
    perceptionManager.init(stackPath+"configs/drone"+cvg_int_to_string(idDrone)+"/perception_manager_configFile.xml");

    return;
}

void PerceptionManagerROSModule::open(ros::NodeHandle & nIn)
{
    init();

    drone_perception_manager_mission_request_pub = nIn.advertise<droneMsgsROS::dronePerceptionManagerMissionRequest>("perception_manager/mission_request", 1, this);
    drone_perception_manager_mission_state_pub = nIn.advertise<droneMsgsROS::dronePerceptionManagerMissionState>("perception_manager/mission_state", 1, this);
    drone_perception_manager_arm_command_pub = nIn.advertise<std_msgs::String>("arm_command/release", 1, this);


    vector_visual_objects_recognized_subs = nIn.subscribe("vector_visual_objects_recognized", 1, &PerceptionManagerROSModule::vectorVisualObjectsRecognizedCallback, this);
    visual_object_recognized_subs = nIn.subscribe("visual_object_recognized", 1, &PerceptionManagerROSModule::visualObjectRecognizedCallback, this);
    drone_estimated_pose_subs = nIn.subscribe("EstimatedPose_droneGMR_wrt_GFF", 1, &PerceptionManagerROSModule::drone_estimated_GMR_pose_callback,this);
    arm_command_subs = nIn.subscribe("perception_manager/arm_command", 1, &PerceptionManagerROSModule::droneArmCommandCallback, this);
    VS_measurement_not_found_subs = nIn.subscribe("VS_measurement_not_found", 1, &PerceptionManagerROSModule::droneVSMeasurementNotFoundCallback, this);
    drone_perception_manager_update_mission_state_subs = nIn.subscribe("perception_manager/update_mission_state", 1, &PerceptionManagerROSModule::dronePerceptionManagerUpdateMissionStateCallback, this);

    perception_manager_update_mission_stateSrv = nIn.advertiseService("perception_manager/update_mission_state_srv",&PerceptionManagerROSModule::perceptionManagerUpdateMissionStateServCall,this);
}

void PerceptionManagerROSModule::drone_estimated_GMR_pose_callback(const droneMsgsROS::dronePose &msg)
{
    last_drone_estimated_GMRwrtGFF_pose  = msg;
    //yaw pitch roll
//    double px_UAV = last_drone_estimated_GMRwrtGFF_pose.x;
//    double py_UAV = last_drone_estimated_GMRwrtGFF_pose.y;
//    double pz_UAV = last_drone_estimated_GMRwrtGFF_pose.z;

//    double yaw = last_drone_estimated_GMRwrtGFF_pose.yaw;
//    double pitch = last_drone_estimated_GMRwrtGFF_pose.pitch;
//    double roll = last_drone_estimated_GMRwrtGFF_pose.roll;
//    perceptionManager.computeHomogeneousMatrixTransform(px_UAV, py_UAV, pz_UAV, yaw, pitch, roll);

    return;
}


void PerceptionManagerROSModule::vectorVisualObjectsRecognizedCallback(const droneMsgsROS::vectorVisualObjectsRecognized &msg)
{
    for(unsigned int i=0;i<msg.objects_recognized.size();i++)
    {
        droneMsgsROS::visualObjectRecognized object_recognized_msg = msg.objects_recognized[i];
        std::vector<bool> object_state = perceptionManager.getObjectState(object_recognized_msg.object_id);
        if(object_state[0] == 1) //Object Allredy Recognized
            return;

        //Extract the last Estimated Pose of the UAV
        double px_UAV = last_drone_estimated_GMRwrtGFF_pose.x;
        double py_UAV = last_drone_estimated_GMRwrtGFF_pose.y;
        double pz_UAV = last_drone_estimated_GMRwrtGFF_pose.z;

        double yaw = last_drone_estimated_GMRwrtGFF_pose.yaw;
        double pitch = last_drone_estimated_GMRwrtGFF_pose.pitch;
        double roll = last_drone_estimated_GMRwrtGFF_pose.roll;
        perceptionManager.computeHomogeneousMatrixTransform(px_UAV, py_UAV, pz_UAV, yaw, pitch, roll);



        //Extract the Estimated Pose of the DETECTED OBJECT
        float recognition_confidence = object_recognized_msg.recognition_confidence;
        visualObjectRecognized_msg.object_id = object_recognized_msg.object_id;
        visualObjectRecognized_msg.object_name = object_recognized_msg.object_name;

        visualObjectRecognized_msg.pose.position.x = object_recognized_msg.pose.position.x;
        visualObjectRecognized_msg.pose.position.y = object_recognized_msg.pose.position.y;
        visualObjectRecognized_msg.pose.position.z = object_recognized_msg.pose.position.z;

        visualObjectRecognized_msg.pose.orientation.x = object_recognized_msg.pose.orientation.x;
        visualObjectRecognized_msg.pose.orientation.y = object_recognized_msg.pose.orientation.y;
        visualObjectRecognized_msg.pose.orientation.z = object_recognized_msg.pose.orientation.z;
        visualObjectRecognized_msg.pose.orientation.w = object_recognized_msg.pose.orientation.w;

        visualObjectRecognized_msg.rotRect.angle = object_recognized_msg.rotRect.angle;
        visualObjectRecognized_msg.rotRect.center.x = object_recognized_msg.rotRect.center.x;
        visualObjectRecognized_msg.rotRect.center.y = object_recognized_msg.rotRect.center.y;
        visualObjectRecognized_msg.rotRect.size.width = object_recognized_msg.rotRect.size.width;
        visualObjectRecognized_msg.rotRect.size.height = object_recognized_msg.rotRect.size.height;


        Eigen::Quaterniond q_E;
        q_E.x() = object_recognized_msg.pose.orientation.x;
        q_E.y() = object_recognized_msg.pose.orientation.y;
        q_E.z() = object_recognized_msg.pose.orientation.z;
        q_E.w() = object_recognized_msg.pose.orientation.w;
        perceptionManager.computeHomogeneousMatrixTransform(object_recognized_msg.pose.position.x, object_recognized_msg.pose.position.y, object_recognized_msg.pose.position.z, q_E);


        //Compute the pose of the DETECTED OBJECT in WORLD COORDINATES
        Eigen::Matrix4d T_obj_world = perceptionManager.computeObjectToWorldTransform();


        //**** We create the CURRENT OBJECT STATE ****
        //The States that we can have for OBJECT RECOGNITION are:
        //state <1, -1, -1> for OBJECT RECOGNIZED
        //state <0, -1, -1> for OBJECT NOT RECOGNIZED
        std::vector<bool> object_current_state = perceptionManager.getObjectState(object_recognized_msg.object_id);
        std::vector<int> state;
        for(unsigned int i=0;i<object_current_state.size();i++)
        {
            if(i == 0) //state for OBJECT RECOGNIZED
                state.push_back(1); //Object has been recognized
            else
                state.push_back(-1);
        }


        bool check_correct_object_height = perceptionManager.validateObjectPose(object_recognized_msg.object_id, T_obj_world);
        if(check_correct_object_height)
            perceptionManager.updateObjectPose(object_recognized_msg.object_id);


        if((recognition_confidence > 0.9) && check_correct_object_height)
        {
            perceptionManager.printNumObjectsDetectionsAlongTime();
            bool num_detections_along_time_reached = false;
            if(object_recognized_msg.object_id == 3)
            {
                if(perceptionManager.getObjectNumDetectionsAlongTime(object_recognized_msg.object_id) >= perceptionManager.getMaxDetectionsAlongTime())
                    num_detections_along_time_reached = true;
            }
            else if(object_recognized_msg.object_id == 1)
            {
                if(perceptionManager.getObjectNumDetectionsAlongTime(object_recognized_msg.object_id) >= (perceptionManager.getMaxDetectionsAlongTime() + 5))
                    num_detections_along_time_reached = true;
            }
            if(num_detections_along_time_reached)
            {
                perceptionManager.updateObjectState(object_recognized_msg.object_id, state);

                int32_t submission_type = perceptionManager.getSubmissionType();
                int32_t submission_state = perceptionManager.getSubmissionState();
                Eigen::Vector3d object_position = perceptionManager.getObjectPosition(object_recognized_msg.object_id);
                Eigen::Quaterniond object_orientation = perceptionManager.getObjectOrientation(object_recognized_msg.object_id);

                dronePerceptionManagerMissionRequest_msg.drone_id = droneId;
                dronePerceptionManagerMissionRequest_msg.object_pose.position.x = object_position(0);
                dronePerceptionManagerMissionRequest_msg.object_pose.position.y = object_position(1);
                dronePerceptionManagerMissionRequest_msg.object_pose.position.z = object_position(2);

                dronePerceptionManagerMissionRequest_msg.object_pose.orientation.x = object_orientation.x();
                dronePerceptionManagerMissionRequest_msg.object_pose.orientation.y = object_orientation.y();
                dronePerceptionManagerMissionRequest_msg.object_pose.orientation.z = object_orientation.z();
                dronePerceptionManagerMissionRequest_msg.object_pose.orientation.w = object_orientation.w();

                dronePerceptionManagerMissionRequest_msg.submission_type = submission_type;
                dronePerceptionManagerMissionState_msg.submission_state = submission_state;

                drone_perception_manager_mission_request_pub.publish(dronePerceptionManagerMissionRequest_msg);
                drone_perception_manager_mission_state_pub.publish(dronePerceptionManagerMissionState_msg);
            }
        }
    }
}

void PerceptionManagerROSModule::visualObjectRecognizedCallback(const droneMsgsROS::visualObjectRecognized &msg)
{
    std::vector<bool> object_state = perceptionManager.getObjectState(msg.object_id);
    if(object_state[0] == 1 || object_state [1] == 1 || object_state [2] == 1) //Object Allredy Recognized or Picked
        return;

    //Extract the last Estimated Pose of the UAV
    double px_UAV = last_drone_estimated_GMRwrtGFF_pose.x;
    double py_UAV = last_drone_estimated_GMRwrtGFF_pose.y;
    double pz_UAV = last_drone_estimated_GMRwrtGFF_pose.z;

    double yaw = last_drone_estimated_GMRwrtGFF_pose.yaw;
    double pitch = last_drone_estimated_GMRwrtGFF_pose.pitch;
    double roll = last_drone_estimated_GMRwrtGFF_pose.roll;
    perceptionManager.computeHomogeneousMatrixTransform(px_UAV, py_UAV, pz_UAV, yaw, pitch, roll);



    //Extract the Estimated Pose of the DETECTED OBJECT
    float recognition_confidence = msg.recognition_confidence;
    visualObjectRecognized_msg.object_id = msg.object_id;
    visualObjectRecognized_msg.object_name = msg.object_name;

    visualObjectRecognized_msg.pose.position.x = msg.pose.position.x;
    visualObjectRecognized_msg.pose.position.y = msg.pose.position.y;
    visualObjectRecognized_msg.pose.position.z = msg.pose.position.z;

    visualObjectRecognized_msg.pose.orientation.x = msg.pose.orientation.x;
    visualObjectRecognized_msg.pose.orientation.y = msg.pose.orientation.y;
    visualObjectRecognized_msg.pose.orientation.z = msg.pose.orientation.z;
    visualObjectRecognized_msg.pose.orientation.w = msg.pose.orientation.w;

    visualObjectRecognized_msg.rotRect.angle = msg.rotRect.angle;
    visualObjectRecognized_msg.rotRect.center.x = msg.rotRect.center.x;
    visualObjectRecognized_msg.rotRect.center.y = msg.rotRect.center.y;
    visualObjectRecognized_msg.rotRect.size.width = msg.rotRect.size.width;
    visualObjectRecognized_msg.rotRect.size.height = msg.rotRect.size.height;


    Eigen::Quaterniond q_E;
    q_E.x() = msg.pose.orientation.x;
    q_E.y() = msg.pose.orientation.y;
    q_E.z() = msg.pose.orientation.z;
    q_E.w() = msg.pose.orientation.w;
    perceptionManager.computeHomogeneousMatrixTransform(msg.pose.position.x, msg.pose.position.y, msg.pose.position.z, q_E);


    //Compute the pose of the DETECTED OBJECT in WORLD COORDINATES
    Eigen::Matrix4d T_obj_world = perceptionManager.computeObjectToWorldTransform();


    //**** We create the CURRENT OBJECT STATE ****
    //The States that we can have for OBJECT RECOGNITION are:
    //state <1, -1, -1> for OBJECT RECOGNIZED
    //state <0, -1, -1> for OBJECT NOT RECOGNIZED
    std::vector<bool> object_current_state = perceptionManager.getObjectState(msg.object_id);
    std::vector<int> state;
    for(unsigned int i=0;i<object_current_state.size();i++)
    {
        if(i == 0) //state for OBJECT RECOGNIZED
            state.push_back(1); //Object has been recognized
        else
            state.push_back(-1);
    }


    bool check_correct_object_height = perceptionManager.validateObjectPose(msg.object_id, T_obj_world);
    if(check_correct_object_height)
        perceptionManager.updateObjectPose(msg.object_id);


    if((recognition_confidence > 0.9) && check_correct_object_height)
    {
        perceptionManager.printNumObjectsDetectionsAlongTime();
        if(perceptionManager.getObjectNumDetectionsAlongTime(msg.object_id) >= perceptionManager.getMaxDetectionsAlongTime())
        {
            generateMissionRequest(msg.object_id, state);
        }
    }

}


void PerceptionManagerROSModule::droneArmCommandCallback(const std_msgs::String &msg)
{
    int32_t submission_state = perceptionManager.getSubmissionState();
    if(submission_state == droneMsgsROS::dronePerceptionManagerMissionState::EXPLORING)
        return;

    std::string arm_command = msg.data;
    if(arm_command == "Release")
        cout<<"Send ARDUINO Release command"<<endl;
    else if(arm_command == "Pick")
        cout<<"Send ARDUINO Pick command"<<endl;

    int object_id;
    int object_in_which_release_id;
    int object_in_which_release_opposite_id;
    std_msgs::String arduino_arm_command_msg;
    switch(submission_state)
    {
        case droneMsgsROS::dronePerceptionManagerMissionState::RELEASE_RED_OBJECT:
            arduino_arm_command_msg.data = "R";
            object_id = 0; // Item A
            object_in_which_release_id = 1; //Bucket A
            object_in_which_release_opposite_id = 3;
            break;
        case droneMsgsROS::dronePerceptionManagerMissionState::RELEASE_BLUE_OBJECT:
            arduino_arm_command_msg.data = "L";
            object_id = 2; // Item B
            object_in_which_release_id = 3; //Bucket B
            object_in_which_release_opposite_id = 1;
            break;

    }

    std::vector<bool> object_current_state = perceptionManager.getObjectState(object_id);
    std::vector<int> state;
    for(unsigned int i=0;i<object_current_state.size();i++)
    {
        if(i == 1) //state for OBJECT PICKED
            state.push_back(0); //Object NOT Picked
        else if(i == 2) //state for OBJECT RELEASED
            state.push_back(1); //Object has been released
        else
            state.push_back(-1);
    }
    perceptionManager.updateObjectState(object_id, state);

    submission_state = droneMsgsROS::dronePerceptionManagerMissionState::EXPLORING;
    dronePerceptionManagerMissionState_msg.submission_state = droneMsgsROS::dronePerceptionManagerMissionState::EXPLORING;


    drone_perception_manager_arm_command_pub.publish(arduino_arm_command_msg);

}

void PerceptionManagerROSModule::droneVSMeasurementNotFoundCallback(const droneMsgsROS::vector3f &msg)
{
    int32_t submission_state = perceptionManager.getSubmissionState();
    if(submission_state == droneMsgsROS::dronePerceptionManagerMissionState::EXPLORING)
        return;


    int object_id;
    switch(submission_state)
    {
        case droneMsgsROS::dronePerceptionManagerMissionState::RELEASE_RED_OBJECT:
            object_id = 1; // Bucket A
            break;
        case droneMsgsROS::dronePerceptionManagerMissionState::RELEASE_BLUE_OBJECT:
            object_id = 3; // Bucket B
            break;
    }

    std::vector<bool> object_current_state = perceptionManager.getObjectState(object_id);
    std::vector<int> state;
    for(unsigned int i=0;i<object_current_state.size();i++)
    {
        if(i == 0) //state for OBJECT RECOGNIZED
            state.push_back(0); //Reset Object Recognized State
        else
            state.push_back(-1);
    }

    perceptionManager.resetObjectPositionsAlongTime(object_id);
    perceptionManager.updateObjectState(object_id, state);

    perceptionManager.setSubmissionState(droneMsgsROS::dronePerceptionManagerMissionState::EXPLORING);
    dronePerceptionManagerMissionState_msg.submission_state = droneMsgsROS::dronePerceptionManagerMissionState::EXPLORING;
    drone_perception_manager_mission_state_pub.publish(dronePerceptionManagerMissionState_msg);

}

void PerceptionManagerROSModule::dronePerceptionManagerUpdateMissionStateCallback(const std_msgs::Int32 &msg)
{
    bool PerceptionManagerRemainingMissionRequest = false;


    //Verify if there is an Object Picked
    std::vector<std::vector<bool> > objects_state = perceptionManager.getObjectsState();
    for(unsigned int i=0;i<objects_state.size();i++)
    {
        if(objects_state[i][1] == 1) //Object Picked
        {
            int object_id = i + 1;
            int num_detections_object = perceptionManager.getObjectNumDetectionsAlongTime(object_id);
            if(num_detections_object > 2)
            {
                PerceptionManagerRemainingMissionRequest = true;
                std::vector<bool> object_current_state = perceptionManager.getObjectState(object_id);
                std::vector<int> state;
                for(unsigned int j=0;j<object_current_state.size();j++)
                {
                    if(j == 0) //state for OBJECT RECOGNIZED
                        state.push_back(1); //Object has been recognized
                    else
                        state.push_back(-1);
                }

                generateMissionRequest(object_id, state);
            }
        }
    }



    if(!PerceptionManagerRemainingMissionRequest)
    {
        perceptionManager.setSubmissionState(droneMsgsROS::dronePerceptionManagerMissionState::EXPLORING);
        dronePerceptionManagerMissionState_msg.submission_state = droneMsgsROS::dronePerceptionManagerMissionState::EXPLORING;
        drone_perception_manager_mission_state_pub.publish(dronePerceptionManagerMissionState_msg);
    }



}


bool PerceptionManagerROSModule::perceptionManagerUpdateMissionStateServCall(droneMsgsROS::perceptionManagerUpdateMissionStateSrv::Request &request, droneMsgsROS::perceptionManagerUpdateMissionStateSrv::Response &response)
{
    bool PerceptionManagerRemainingMissionRequest = false;


    //Verify if there is an Object Picked
    std::vector<std::vector<bool> > objects_state = perceptionManager.getObjectsState();
    for(unsigned int i=0;i<objects_state.size();i++)
    {
        if(objects_state[i][1] == 1) //Object Picked
        {
            int object_id = i + 1;
            int num_detections_object = perceptionManager.getObjectNumDetectionsAlongTime(object_id);
            if(num_detections_object > 2)
            {
                PerceptionManagerRemainingMissionRequest = true;
                std::vector<bool> object_current_state = perceptionManager.getObjectState(object_id);
                std::vector<int> state;
                for(unsigned int j=0;j<object_current_state.size();j++)
                {
                    if(j == 0) //state for OBJECT RECOGNIZED
                        state.push_back(1); //Object has been recognized
                    else
                        state.push_back(-1);
                }

                generateMissionRequest(object_id, state);
            }
        }
    }



    if(!PerceptionManagerRemainingMissionRequest)
    {
        perceptionManager.setSubmissionState(droneMsgsROS::dronePerceptionManagerMissionState::EXPLORING);
        dronePerceptionManagerMissionState_msg.submission_state = droneMsgsROS::dronePerceptionManagerMissionState::EXPLORING;
        drone_perception_manager_mission_state_pub.publish(dronePerceptionManagerMissionState_msg);
    }

    response.ack = PerceptionManagerRemainingMissionRequest;

}


void PerceptionManagerROSModule::generateMissionRequest(unsigned int object_id, std::vector<int> &new_state)
{
    droneMsgsROS::dronePerceptionManagerMissionRequest PerceptionManagerMissionRequest_msg;
    droneMsgsROS::dronePerceptionManagerMissionState PerceptionManagerMissionState_msg;
    perceptionManager.updateObjectState(object_id, new_state);

    int32_t submission_type = perceptionManager.getSubmissionType();
    int32_t submission_state = perceptionManager.getSubmissionState();
    Eigen::Vector3d object_position = perceptionManager.getObjectPosition(object_id);
    Eigen::Quaterniond object_orientation = perceptionManager.getObjectOrientation(object_id);

    PerceptionManagerMissionRequest_msg.drone_id = droneId;
    PerceptionManagerMissionRequest_msg.object_pose.position.x = object_position(0);
    PerceptionManagerMissionRequest_msg.object_pose.position.y = object_position(1);
    PerceptionManagerMissionRequest_msg.object_pose.position.z = object_position(2);
    PerceptionManagerMissionRequest_msg.object_pose.orientation.x = object_orientation.x();
    PerceptionManagerMissionRequest_msg.object_pose.orientation.y = object_orientation.y();
    PerceptionManagerMissionRequest_msg.object_pose.orientation.z = object_orientation.z();
    PerceptionManagerMissionRequest_msg.object_pose.orientation.w = object_orientation.w();
    PerceptionManagerMissionRequest_msg.submission_type = submission_type;

    PerceptionManagerMissionState_msg.submission_state = submission_state;

    if(submission_state != PerceptionManager::SubmissionState::Exploring)
        drone_perception_manager_mission_request_pub.publish(PerceptionManagerMissionRequest_msg);
    drone_perception_manager_mission_state_pub.publish(PerceptionManagerMissionState_msg);
}


