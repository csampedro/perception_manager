#include "PerceptionManagerROSModule.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <cmath>
using namespace std;



int main(int argc, char **argv)
{
    ros::init(argc, argv, "Perception_Manager");
    ros::NodeHandle n;

    PerceptionManagerROSModule myPerceptionManagerROSModule;
    myPerceptionManagerROSModule.open(n);


    while(ros::ok())
    {
        ros::spin();
    }
    return 0;
}


