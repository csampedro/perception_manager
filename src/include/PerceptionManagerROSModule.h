
#ifndef _PERCEPTION_MANAGER_ROS_MODULE_H
#define _PERCEPTION_MANAGER_ROS_MODULE_H

#include <iostream>
#include <string>
#include <vector>
#include "PerceptionManager.h"
#include "droneModuleROS.h"
#include "xmlfilereader.h"

// ROS
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "droneMsgsROS/visualObjectRecognized.h"
#include "droneMsgsROS/vectorVisualObjectsRecognized.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/dronePerceptionManagerMissionRequest.h"
#include "droneMsgsROS/dronePerceptionManagerMissionState.h"
#include "droneMsgsROS/vector3f.h"
#include "std_msgs/Int32.h"
#include "droneMsgsROS/perceptionManagerUpdateMissionStateSrv.h"


class PerceptionManagerROSModule : public DroneModule
{
private:

    PerceptionManager perceptionManager;
    int droneId;
    //ROS Publishers
    ros::Publisher drone_perception_manager_mission_request_pub;
    ros::Publisher drone_perception_manager_mission_state_pub;
    ros::Publisher drone_perception_manager_arm_command_pub;

    //ROS Subscribers
    ros::Subscriber vector_visual_objects_recognized_subs;
    ros::Subscriber visual_object_recognized_subs;
    ros::Subscriber drone_estimated_pose_subs;
    ros::Subscriber arm_command_subs;
    ros::Subscriber VS_measurement_not_found_subs;
    ros::Subscriber drone_perception_manager_update_mission_state_subs;

    //ROS Services
    ros::ServiceServer perception_manager_update_mission_stateSrv;


    //ROS Messages
    droneMsgsROS::visualObjectRecognized visualObjectRecognized_msg;
    droneMsgsROS::dronePose last_drone_estimated_GMRwrtGFF_pose;
    droneMsgsROS::dronePerceptionManagerMissionRequest dronePerceptionManagerMissionRequest_msg;
    droneMsgsROS::dronePerceptionManagerMissionState dronePerceptionManagerMissionState_msg;

public:
    void open(ros::NodeHandle & nIn);
    void init();
    void generateMissionRequest(unsigned int object_id, std::vector<int> &new_state);

    //ROS callbacks
    void vectorVisualObjectsRecognizedCallback(const droneMsgsROS::vectorVisualObjectsRecognized &msg);
    void visualObjectRecognizedCallback(const droneMsgsROS::visualObjectRecognized &msg);
    void drone_estimated_GMR_pose_callback(const droneMsgsROS::dronePose &msg);
    void droneArmCommandCallback(const std_msgs::String &msg);
    void droneVSMeasurementNotFoundCallback(const droneMsgsROS::vector3f &msg);
    void dronePerceptionManagerUpdateMissionStateCallback(const std_msgs::Int32 &msg);
    bool perceptionManagerUpdateMissionStateServCall(droneMsgsROS::perceptionManagerUpdateMissionStateSrv::Request& request, droneMsgsROS::perceptionManagerUpdateMissionStateSrv::Response& response);


    PerceptionManagerROSModule();
    ~PerceptionManagerROSModule();


};


#endif
