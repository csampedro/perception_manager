
#ifndef _PERCEPTION_MANAGER_H_
#define _PERCEPTION_MANAGER_H_

#include <math.h>
#include <numeric>
#include <Eigen/Dense>
#include <Eigen/Geometry>
#include "pugixml.hpp"
#include "ListOfCylindricalObjects.h"
using namespace std;


#define MAX_SURFACE_HEIGHT 1.0
#define TOLERANCE_HEIGHT 0.15


class PerceptionManager
{
private:
    ListOfCylindricalObjects listOfCylindricalObjects;
    std::vector<std::vector<bool> > current_objects_state;
    int32_t submission_type;
    int32_t submission_state;
    double camera_pitch_angle;
    Eigen::Vector3d camera_offset;
    Eigen::Matrix4d T_uav_world;
    Eigen::Matrix4d T_obj_cam;
    Eigen::Matrix4d T_obj_world;
    std::vector<unsigned int> num_objects_detections_along_time;
    int MAX_OBJECT_DETECTIONS_ALONG_TIME;

public:
    enum SubmissionType
    {
        GoForGrasping = 1,
        GoForReleasing = 2,
    };
    enum SubmissionState
    {
        Exploring = 1,
        GoForGraspingRedObject = 2,
        GoForGraspingBlueObject = 3,
        GoForReleasingRedObject = 4,
        GoForReleasingBlueObject = 5,
    };
    bool readConfigs(const std::string &configFile);
    bool validateObjectPose(const int object_id, const Eigen::Matrix4d &Tmat);
    void resetObjectPositionsAlongTime(const unsigned int object_id){listOfCylindricalObjects.resetObjectDetectionsAlongTime(object_id);}
    void updateObjectState(const unsigned int object_id, const std::vector<int> &new_state);
    void updateObjectPose(const unsigned int object_id);
    void printCurrentObjectsState();
    void generateSubmission(const unsigned int object_id, const std::vector<int> &new_state);
    void init(const std::string configFile);
    void printNumObjectsDetectionsAlongTime();
    inline std::vector<unsigned int> getNumObjectsDetectionsAlongTime(){listOfCylindricalObjects.getNumObjectsDetectionsAlongTime();}
    inline int getObjectNumDetectionsAlongTime(const unsigned int object_id){return listOfCylindricalObjects.getObjectNumDetectionsAlongTime(object_id);}
    inline int32_t getSubmissionType(){return submission_type;}
    inline int32_t getSubmissionState(){return submission_state;}
    inline void setSubmissionState(const int32_t state){submission_state = state;}
    inline std::vector<std::vector<bool> > getObjectsState(){return listOfCylindricalObjects.getObjectsState();}
    inline std::vector<bool> getObjectState(const unsigned int object_id){return listOfCylindricalObjects.getObjectState(object_id);}
    inline Eigen::Vector3d getObjectPosition(const unsigned int object_id){return listOfCylindricalObjects.getObjectPosition(object_id);}
    inline Eigen::Quaterniond getObjectOrientation(const unsigned int object_id){return listOfCylindricalObjects.getObjectOrientation(object_id);}
    inline std::vector<std::vector<bool> > getCurrentObjectsState(){return current_objects_state;}
    inline int getMaxDetectionsAlongTime(){return MAX_OBJECT_DETECTIONS_ALONG_TIME;}
    Eigen::Matrix3d convertFromRPYtoRotationMatrix(const float alpha, const float beta, const float gamma);
    Eigen::Matrix4d computeHomogeneousMatrixTransform(const float px, const float py, const float pz, const float alpha, const float beta, const float gamma);
    Eigen::Matrix4d computeHomogeneousMatrixTransform(const float px, const float py, const float pz, const Eigen::Quaterniond q_E);
    Eigen::Matrix4d computeObjectToWorldTransform();
    void computeObjectDefinitivePosition(const unsigned int object_id){listOfCylindricalObjects.computeObjectDefinitivePosition(object_id);}
    PerceptionManager(void);
    ~PerceptionManager(void);
};

#endif
