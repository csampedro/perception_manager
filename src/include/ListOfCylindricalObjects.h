#ifndef LISTOFCYLINDRICALOBJECTS_H
#define LISTOFCYLINDRICALOBJECTS_H

#include <numeric>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include "opencv2/opencv.hpp"
#include "pugixml.hpp"
#include "CylindricalObject.h"
using namespace std;

#define MAX_OBJECTS 10

class ListOfCylindricalObjects
{
private:
    CylindricalObject *objects_list[MAX_OBJECTS];
    unsigned int number_of_objects;
public:
    void init(string configFile);
    bool readConfigs(string &configFile);
    void Delete(CylindricalObject *d);
    void Delete(int index);
    void DestroyContent();
    bool Add(CylindricalObject *d);
    void resetObjectDetectionsAlongTime(const unsigned int object_id);
    void updateObjectState(const unsigned int object_id, const std::vector<int> &state);
    void updateObjectPose(const unsigned int object_id, const Eigen::Vector3d p_E, const Eigen::Quaterniond q_E);
    void printObjectsState();
    void computeObjectDefinitivePosition(const unsigned int object_id);
    std::vector<bool> getObjectState(const unsigned int object_id);
    std::vector<std::vector<bool> > getObjectsState();
    std::vector<unsigned int> getNumObjectsDetectionsAlongTime();
    inline unsigned int getNumberOfObjects(){return number_of_objects;}
    inline unsigned int getObjectNumDetectionsAlongTime(const unsigned int object_id){return objects_list[object_id]->getNumDetectionsAlongTime();}
    inline unsigned int getNumberOfObjectStates(const unsigned int object_id){return objects_list[object_id]->getNumObjectStates();}
    inline Eigen::Vector3d getObjectPosition(const unsigned int object_id){return objects_list[object_id]->getPosition();}
    inline Eigen::Quaterniond getObjectOrientation(const unsigned int object_id){return objects_list[object_id]->getOrientation();}
    ListOfCylindricalObjects();
    ~ListOfCylindricalObjects();
};

#endif // LISTOFCYLINDRICALOBJECTS_H
