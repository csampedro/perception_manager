#ifndef CYLINDRICALOBJECT_H
#define CYLINDRICALOBJECT_H

#include <Eigen/Dense>
#include <algorithm>
#include <numeric>
#include "opencv2/opencv.hpp"
using namespace std;

#define MAX_OBJECT_POSES_ALONG_TIME 1000

class CylindricalObject
{
private:
    //    bool object_detected_flag;
    //    bool object_picked_flag;
    //    bool object_released_flag;
    float radius, height;
    Eigen::Vector3d position;
    Eigen::Quaterniond orientation;
    unsigned int object_id;
    std::string object_color;
    std::vector<bool> object_state;
    unsigned int num_object_states; //{Recognized, Picked, Released}
    std::vector<Eigen::Vector3d> positions_along_time;

public:
//    void setObjectDetectedFlag(bool detected_flag){object_detected_flag = detected_flag;}
//    void setObjectPickedFlag(bool picked_flag){object_picked_flag = picked_flag;}
//    void setObjectReleasedFlag(bool released_flag){object_released_flag = released_flag;}

    Eigen::Vector3d computeMedian();
    Eigen::Vector3d computeMean(const std::vector<Eigen::Vector3d> &vec_pos);
    Eigen::Vector3d computeBestMeasurement(const std::vector<Eigen::Vector3d> &vec_pos);
    void printObjectState();
    void updateObjectState(const std::vector<int> &state);
    void updateObjectPose(const Eigen::Vector3d p_E, const Eigen::Quaterniond q_E);
    void setObjectId(unsigned int id){object_id = id;}
    void setRadius(float r){radius = r;}
    void setHeight(float h){height = h;}
    void setColor(std::string color){object_color = color;}
    void setPosition(double x, double y, double z);
    void setOrientation(Eigen::Quaterniond &q_E){orientation = q_E;}
    void computeDefinitivePosition();
    Eigen::Vector3d getPosition();
    inline Eigen::Quaterniond getOrientation(){return orientation;}
    inline std::string getColor(){return object_color;}
    inline float getRadius(){return radius;}
    inline float getHeight(){return height;}
    inline unsigned int getObjectId(){return object_id;}
    inline std::vector<bool> getObjectState(){return object_state;}
    inline unsigned int getNumDetectionsAlongTime(){return positions_along_time.size();}
    unsigned int getNumObjectStates(){return num_object_states;}
    void resetObjectPositionsAlongTime();
    CylindricalObject();
    CylindricalObject(float r, float h, unsigned int id, std::string color);
    CylindricalObject(float r, float h, unsigned int id, std::string color, std::vector<bool> default_states);
    ~CylindricalObject(void);
};

#endif // CYLINDRICALOBJECT_H
